resource "google_compute_instance" "vm_instance" {
  name          = "node${count.index + 1}"
  machine_type  = "e2-small"
  count = 3
  boot_disk {
        initialize_params {
                image = "debian-cloud/debian-10"
                size  = 20
        }
    }
        network_interface {
        network = "default"
        access_config {
        }
     }

}
